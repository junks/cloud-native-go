package api

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	Success bool `json:"success"`
	Message string `json:"message"`
}

func ApiHandler(w http.ResponseWriter, req *http.Request) {
	json.NewEncoder(w).Encode(Response{true, "return from api handler"})
}

func EchoHandler(w http.ResponseWriter, req *http.Request) {
	var object struct{
		Message string `json:"message"`
	}

	err := json.NewDecoder(req.Body).Decode(&object)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(Response{Success:false, Message:err.Error()})
		return
	}

	json.NewEncoder(w).Encode(Response{Success:true, Message: object.Message})
}