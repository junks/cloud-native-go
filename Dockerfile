FROM golang:1.7.4-alpine
MAINTAINER Huyen Vu

ENV SOURCES /go/src/cloud-native-go

COPY . ${SOURCES}

RUN cd ${SOURCES} && CGO_ENABLED=0 go install

ENV PORT 8080
EXPOSE ${PORT}

ENTRYPOINT cloud-native-go