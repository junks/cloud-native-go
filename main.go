package main

import (
	"cloud-native-go/api"
	"fmt"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/api", api.ApiHandler)
	http.HandleFunc("/echo", api.EchoHandler)
	fmt.Println("Server is listening on port: ", getPort())
	panic(http.ListenAndServe(getPort(), nil))
}

func getPort() string{
	port := os.Getenv("PORT")
	if len(port) > 0 {
		return ":"+port
	}
	return ":8080"
}
